data = [
    ["January", 6, 3],
    ["February", 7, 3],
    ["March", 10, 4],
    ["April", 13, 6],
    ["May", 17, 9],
    ["June", 20, 12],
    ["July", 22, 14],
    ["August", 21, 14],
    ["September", 19, 12],
    ["October", 14, 9],
    ["November", 10, 6],
    ["December", 7, 3]
]

sorted_data = sorted(data, key=lambda i:i[1], reverse=True)

print("6 months with highest temperatures:")
for i in sorted_data[:5]:
    print(f"{i[0]}: {i[1]}°C (max)")

print()
print("Months with minimum temperature < 9°C:")
for i in sorted_data:
    if i[2] < 9:
        print(f"{i[0]}: {i[1]}°C (max) {i[2]}°C (min)")

print()
print("Months with maximum temperature >= 20°C:")
for i in sorted_data:
    if i[1] >= 20:
        print(f"{i[0]}: {i[1]}°C (max) {i[2]}°C (min)")