import random

number = random.randrange(500)
guess = int(input("Enter guess: "))
count = 0

while guess != number and count < 8:
    if guess > number:
        print("Too high.")
    else:
        print("Too low.")
    count += 1
    guess = int(input("Enter guess: "))

if guess == number:
    print("Well done - you guessed the correct number.")
else:
    print("Sorry - game over. The number was", str(number))